export const mainColor = {
    baseColor: '#DC0B16',
    text: '#fff',
    inputBorder: '#DC0B16',
    inactivePlaceholderText: 'gray'
}