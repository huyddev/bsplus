import React from 'react'
import { TouchableWithoutFeedback, View, TextInput } from 'react-native';
import { Icon } from 'react-native-elements';
import PropTypes from 'prop-types'
import styles from "./styles";

export default class CustomInput extends React.Component {

    static propTypes = {
        leftIcon: PropTypes.string,
        rightIcon: PropTypes.string,
        transparent: PropTypes.bool,
        propComponent: PropTypes.object,
        style: PropTypes.object,
        onChangeText: PropTypes.func,
        focusStyle: PropTypes.object,
        isDot: PropTypes.bool
    }

    constructor(props) {
        super(props);
        this.state = {
            isFocused: false
        }
    }

    _onFocus() {
        this.setState({ isFocused: true })
        if (this.props.onFocus) this.props.onFocus();
    }

    _onBlur() {
        this.setState({ isFocused: false });
        if (this.props.onBlur) this.props.onBlur();
    }

    render() {
        const { isDot, leftIcon, rightIcon, propComponent, style, onChangeText, focusStyle, transparent } = this.props;
        let { isFocused } = this.state;
        return (
            <View style={[styles.container, style, transparent ? styles.transparent : {}, (isFocused && focusStyle) ? focusStyle : {}]}>
                {leftIcon &&
                    <Icon
                        name='ios-search'
                        type='ionicon'
                        color='#fff'
                        size={22}
                    />
                }
                <TextInput
                    style={styles.input}
                    underlineColorAndroid="transparent"
                    {...propComponent}
                    onChangeText={(value) => onChangeText(value)}
                    onFocus={() => this._onFocus()}
                    onBlur={() => this._onBlur()} />
                {rightIcon &&
                    <Icon
                        name={rightIcon}
                        type='ionicon'
                        color='#fff'
                    />
                }
            </View>
        )
    }
}