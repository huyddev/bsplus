import React from 'react';
import { Icon } from 'react-native-elements';
import { Text, View, TextInput } from 'react-native';
import styles from './styles';
import { mainColor } from '../../../utils/styleUtils';

export default class Search extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Icon
                    name='ios-search'
                    type='ionicon'
                    color={mainColor.baseColor}
                    size={20}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Nhập để tìm kiếm" />
            </View>
        )
    }
}