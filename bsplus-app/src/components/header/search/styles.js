export default {
    container: {
        flex: 1,
        paddingHorizontal: 15,
        borderRadius: 13,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    input: {
        marginLeft: 5
    }
}